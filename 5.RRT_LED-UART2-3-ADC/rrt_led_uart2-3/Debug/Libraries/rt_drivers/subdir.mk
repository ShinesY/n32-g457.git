################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Libraries/rt_drivers/drv_adc.c \
../Libraries/rt_drivers/drv_common.c \
../Libraries/rt_drivers/drv_gpio.c \
../Libraries/rt_drivers/drv_pwm.c \
../Libraries/rt_drivers/drv_usart.c 

O_SRCS += \
../Libraries/rt_drivers/drv_common.o \
../Libraries/rt_drivers/drv_gpio.o \
../Libraries/rt_drivers/drv_pwm.o \
../Libraries/rt_drivers/drv_usart.o 

OBJS += \
./Libraries/rt_drivers/drv_adc.o \
./Libraries/rt_drivers/drv_common.o \
./Libraries/rt_drivers/drv_gpio.o \
./Libraries/rt_drivers/drv_pwm.o \
./Libraries/rt_drivers/drv_usart.o 

C_DEPS += \
./Libraries/rt_drivers/drv_adc.d \
./Libraries/rt_drivers/drv_common.d \
./Libraries/rt_drivers/drv_gpio.d \
./Libraries/rt_drivers/drv_pwm.d \
./Libraries/rt_drivers/drv_usart.d 


# Each subdirectory must supply rules for building sources it contributes
Libraries/rt_drivers/%.o: ../Libraries/rt_drivers/%.c
	arm-none-eabi-gcc -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\applications\my_appp" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\applications\my_appp" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\Libraries\N32_Std_Driver\CMSIS\core" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\Libraries\N32_Std_Driver\CMSIS\device" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\Libraries\N32_Std_Driver\n32g45x_std_periph_driver\inc" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\Libraries\rt_drivers" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\applications" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\board\msp" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\board" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\rt-thread\components\drivers\include" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\rt-thread\components\finsh" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\rt-thread\components\libc\compilers\common" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\rt-thread\components\libc\compilers\gcc\newlib" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\rt-thread\include" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\rt-thread\libcpu\arm\common" -I"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\rt-thread\libcpu\arm\cortex-m4" -include"D:\1.MY_Data\2.N32G45XVL\5.RRT_LED-UART2-3-ADC\rrt_led_uart2-3\rtconfig_preinc.h" -std=gnu11 -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -ffunction-sections -fdata-sections -Dgcc -O0 -gdwarf-2 -g -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"

