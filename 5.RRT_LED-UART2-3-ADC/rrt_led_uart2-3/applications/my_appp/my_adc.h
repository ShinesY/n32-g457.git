/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-16     BOLU       the first version
 */
#ifndef MY_ADC_H_
#define MY_ADC_H_
#include <rtdef.h>


rt_uint32_t adc_vol(void);

int adc_Pc0init(void);

#endif /* MY_ADC_H_ */
