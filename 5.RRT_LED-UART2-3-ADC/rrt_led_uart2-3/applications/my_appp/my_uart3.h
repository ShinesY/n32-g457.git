/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-15     BOLU       the first version
 */
#ifndef MY_UART3_H_
#define MY_UART3_H_

int uart3_dam_sample(void);

#endif /* MY_UART3_H_ */
