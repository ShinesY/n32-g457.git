/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-09     BOLU       the first version
 */
#include "my_pwm.h"
#include <n32_msp.h>
#include <n32g45x.h>
#include <rtthread.h>
#include <rtdevice.h>
#include "my_adc.h"

#define PWM_DEV_NAME        "time3pwm2"  /* PWM设备名称 */
#define PWM_DEV_CHANNEL     2       /* PWM通道 */

struct rt_device_pwm *pwm_dev;      /* PWM设备句柄 */


#define THREAD_PRIORITY         25  //线程优先级
#define THREAD_STACK_SIZE       512 //线程大小
#define THREAD_TIMESLICE        5   //线程时间片

#define closPin      54             //pb15

rt_uint32_t period, pulse, dir;

int pwm_led2_sample(void)
{
    period = 1000;     //周期为1ms，单位为纳秒ns   50000
    dir = 1;            // PWM脉冲宽度值的增减方向
    pulse = 0;          // PWM脉冲宽度值，单位为纳秒ns

    // 查找设备
    pwm_dev = (struct rt_device_pwm *)rt_device_find(PWM_TEST_NAME_CH_2);
    if (pwm_dev == RT_NULL)
    {
        rt_kprintf("pwm sample run failed! can't find %s device!\n", PWM_TEST_NAME_CH_2);
        return RT_ERROR;
    }
    else
    {
        rt_kprintf("pwm sample run successful!\n");
    }
     //设置PWM周期和脉冲宽度默认值
    rt_pwm_set(pwm_dev, PWM_DEV_CHANNEL, period, pulse);
     //使能设备
    rt_pwm_enable(pwm_dev, PWM_DEV_CHANNEL);

    return RT_EOK;
}

/*void pwm_thread1_entry(void *parameter)
{
    while (1)
    {
        rt_thread_mdelay(50);
        if (dir)
        {
            pulse += 50; // 从0值开始每次增加5000ns
        }
        else
        {
            pulse -= 50; // 从最大值开始每次减少5000ns
        }
        if (pulse >= period)
        {
            dir = 0;
        }
        if (0 == pulse)
        {
            dir = 1;
        }

        // 设置PWM周期和脉冲宽度
        rt_pwm_set(pwm_dev, PWM_DEV_CHANNEL, period, pulse);

    }
}*/
void pwm_thread1_entry(void *parameter)
{
    rt_uint32_t stast = 0;
    while (1)
    {
        rt_thread_mdelay(50);
        stast = adc_vol();
        if (stast > 300)
        {
            rt_pwm_set(pwm_dev, PWM_DEV_CHANNEL, period, pulse);
            pulse += 20; // 从0值开始每次增加5000ns
//            rt_kprintf("stast = %d\n",stast);
            // 设置PWM周期和脉冲宽度
        }
        else {
            rt_pwm_set(pwm_dev, PWM_DEV_CHANNEL, period, 0);
        }
        if (pulse >= period)
        {
            pulse = period;
            rt_pin_write(closPin,PIN_HIGH);

        }
        // 设置PWM周期和脉冲宽度
//        rt_pwm_set(pwm_dev, PWM_DEV_CHANNEL, period, pulse);

    }
}


int pwm_init(void)
{
    rt_thread_t tid1 = RT_NULL;


    rt_pin_mode(closPin, PIN_MODE_OUTPUT_OD );
    rt_pin_write(closPin,PIN_LOW);

    n32_msp_tim_init(TIM3);
    pwm_led2_sample();//初始化pwm

    /* 创建线程 1，名称是 thread1，入口是 thread1_entry*/
    tid1 = rt_thread_create("thread1", pwm_thread1_entry, RT_NULL,THREAD_STACK_SIZE,THREAD_PRIORITY, THREAD_TIMESLICE);
    //启动线程，开始调度
    if (tid1 != RT_NULL)
    {
        rt_thread_startup(tid1);
        rt_kprintf("pwm thread1 run successful!\n");
    }
    else
    {
        return -RT_ERROR;
    }
}








